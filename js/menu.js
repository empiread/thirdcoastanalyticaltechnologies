////////////////////////////////////////////////////////////////////////////////////////////////////
// PB USAGE: Just place the params on a div tag wrapped around the <ul id="menu">.. If none, it uses default parameters.. (see below)
// EXAMPLE:
/*
<div id="menu_wrap" params="1,-1,500,fade,200,h"> 
	<ul id="menu">
*/
////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){	
	// add classes for submenu arrows	
	$("#menu > li:has(ul)").addClass("sub");
	$("#menu > li > ul > li:has(ul)").addClass("hassub");
	$("#menu > li > ul > li > ul > li:has(ul)").addClass("hassub");
	$("#menu > li > ul > li > ul > li > ul > li:has(ul)").addClass("hassub");
	// add classes for submenu no arrows
	$("#menu > li > ul > li:not(:has(ul))").addClass("nosub");
	$("#menu > li > ul > li > ul > li:not(:has(ul))").addClass("nosub");
	$("#menu > li > ul > li > ul > li > ul > li:not(:has(ul))").addClass("nosub");
	$("#menu > li > ul > li > ul > li > ul > li > ul > li:not(:has(ul))").addClass("nosub");
	/* initiate menu */
	PBddminit();
});

////////////////////////////////////////////////////////////////////////////////////////////////////	
var _debug = 1;
if(!mlddm_effect)
{
	var mlddm_shiftx		= 0;
	var mlddm_shifty		= 0;
	var mlddm_timeout		= 500;
	var mlddm_effect		= 'none';
	var mlddm_effect_speed	= 300;
	var mlddm_orientation	= 'h';
	var mlddm_direction		= true;
	var mlddm_md			= 375;
}
var PBDDM_ID		= 'menu';
var obj_menu		= new Array();
function PBddminit(md7)
{
	var candidates = $('ul');
	var index = 0;
	for(var i=0; i < candidates.length; i++)
	{
		if(candidates[i].id == PBDDM_ID)
		{
			$(candidates[i]).css('visibility','visible');
			$(candidates[i]).addClass('clearfix');
			var obj = candidates[i];
			var value = $(obj).parent().attr('params'); // get parent containers attributes.. if none it defaults to none..
			obj_menu[index] = new menu(obj, index, value);
			index++;
		}
	}
}
function layer(handler)
{
	this.handler		= handler;
	this.showed			= false;
	this.level			= 0;
	this.outerwidth		= 0;
	this.outerheight	= 0;
	this.innerwidth		= 0;
	this.innerheight	= 0;
	this.x				= 0;
	this.y				= 0;
	this.border			= 0;
	this.topmargin		= 0;
	this.shifter		= 0;
	this.parentindex	= 0;
	this.reverse		= false;
	this.timeouts		= new Array();
	this.degree			= 0;
	this.csstext		= '';
}
function menu(obj, obj_n, params)
{
	var _handler			= obj;
	var _obj_num			= obj_n;
	var _me					= this;
	var _closetimer			= null;
	var _mouseout			= true;
	var _currentlayer		= null;
	var _shiftx				= mlddm_shiftx;
	var _shifty				= mlddm_shifty;
	var _timeout			= mlddm_timeout;
	var _effect				= mlddm_effect;
	var _effect_speed		= mlddm_effect_speed;
	var _orientation		= mlddm_orientation;
	var _direction			= mlddm_direction;
	var params_array;
	if(params)
	{
		params_array = params.split(",");
		if(params_array[0]) _shiftx       = params_array[0]*1;
		if(params_array[1]) _shifty       = params_array[1]*1;
		if(params_array[2]) _timeout      = params_array[2]*1;
		if(params_array[3]) _effect		  = params_array[3];
		if(params_array[4]) _effect_speed = params_array[4]*1;
		if(params_array[5]) _orientation  = params_array[5];
		if(params_array[6]) _direction    = params_array[6]*1;
		if(!_effect_speed) _effect_speed = 1000;
	}
	this._layer = new Array();
	function opacity(index, opac_start, opac_end, speed)
	{
		var current_layer = _me._layer[index];
		for(var z = 0; z < current_layer.timeouts.length; z++) clearTimeout(current_layer.timeouts[z]);
		var degree = current_layer.degree;
		var speed = Math.round(1000/speed);
		var timer = 0;
		if(degree < opac_end)
		{
			for(var i = degree; i <= opac_end; i = i + 4)
			{
				current_layer.timeouts[timer] = setTimeout("changeOpac(" + _obj_num + "," + index + "," + i + ")", (timer * speed));
				timer++;
			}
		}
		else if(degree > opac_end)
		{
			for(var i = degree; i >= opac_end; i = i - 4)
			{
				current_layer.timeouts[timer] = setTimeout("changeOpac(" + _obj_num + "," + index + "," + i + ")", (timer * speed));
				timer++;
			}
		}
	}
	function slide(index, direction, speed)
	{
		var current_layer = _me._layer[index];
		for(var z = 0; z < current_layer.timeouts.length; z++) clearTimeout(current_layer.timeouts[z]);
		var degree = current_layer.degree;
		var speed = Math.round(1000/speed);
		var timer = 0;
		if(_orientation == 'h')	_ori = 0;
		else					_ori = 1;
		if(direction == 'show')
		{
			for(i = degree; i <= 100; i = i + 2)
			{
				current_layer.timeouts[timer] = setTimeout("changePOS(" + _obj_num + "," + index + "," + i + "," + _ori + ")", (timer * speed));
				timer++;
			}
		}
		else if(direction == 'hide')
		{
			for(i = degree; i >= 0; i = i - 2)
			{
				current_layer.timeouts[timer] = setTimeout("changePOS(" + _obj_num + "," + index + "," + i + "," + _ori + ")", (timer * speed));
				timer++;
			}
		}
	}
	function mopen(index)
	{
		if(!_me._layer[index].showed && (mlddm_md==375))
		{
			if(_effect == 'fade')		opacity(index, 0, 100, _effect_speed);
			else if(_effect == 'slide')	slide(index, 'show', _effect_speed);
			else						_me._layer[index].handler.style.visibility = 'visible';
			button_on(_me._layer[index].handler);
			_me._layer[index].showed = true;
		}
	}
	function mclose(index)
	{
		if(_me._layer[index].showed)
		{
			if(_effect == 'fade')		opacity(index, 100, 0, _effect_speed);
			else if(_effect == 'slide')	slide(index, 'hide', _effect_speed);
			else						_me._layer[index].handler.style.visibility = 'hidden';
			button_off(_me._layer[index].handler);
			_me._layer[index].showed = false;
		}
	}
	function getlevel(layer)
	{
		var level = 0;
		var currentobj = layer;
		while(currentobj.id != PBDDM_ID)
		{
			if(currentobj.nodeName == 'UL') level++;
			currentobj = currentobj.parentNode;
		}
		return level;
	}
	function getbutton(layer)
	{
		var button;
		var currobj = layer;
		var index = 0;
		while(currobj.id != PBDDM_ID)
		{
			if(currobj.nodeName == 'LI')
			{
				index++;
				button = currobj;
			}
			currobj = currobj.parentNode;
		}
		return button;
	}
	function button_on(layer)
	{
		if(getlevel(layer) !=1) return -1;
		var button = getbutton(layer);
		if(button)
		{
			button = button.getElementsByTagName("a")[0];
			button.id = 'buttonhover';
		}
	}
	function button_off(layer)
	{
		if(getlevel(layer) !=1) return -1;
		var button = getbutton(layer);
		if(button)
		{
			button = button.getElementsByTagName("a")[0];
			button.id = 'buttonnohover';
		}
	}
	function getlayerindex(obj)
	{
		for(i = 0; i < _me._layer.length; i++) { if(_me._layer[i].handler == obj) return i; }
		return -1;
	}
	function getparentindex(layer)
	{
		while(layer.id != PBDDM_ID)
		{
			layer = layer.parentNode;
			if(layer.nodeName == 'UL') return getlayerindex(layer);
		}
		return -1;
	}
	function gettopmargin(obj)
	{
		var top = obj.offsetTop;
			obj.style.marginTop = '0px';
		var margintop = top - obj.offsetTop;
			obj.style.marginTop = margintop+'px';
		return margintop;
	}
	function getparentheight(layer)
	{
		while(layer.id != PBDDM_ID)
		{
			layer = layer.parentNode;
			if(layer.nodeName == 'LI') break;
		}
		return layer.getElementsByTagName("a")[0].offsetHeight;
	}
	function closeall() { for(var i=0; i < _me._layer.length; i++) { mclose(i); }}
	function mclosetime() {	_closetimer = window.setTimeout(closeall, _timeout); }
	function mcancelclosetime() { if(_closetimer) { window.clearTimeout(_closetimer); _closetimer = null; }}
	function setpositions(client_width, scroll_left)
	{
		var max_right = client_width + scroll_left;
		for(var i = 0; i < _me._layer.length; i++)
		{
			if(_me._layer[i].level > 1)
			{
				_me._layer[i].handler.style.left = _me._layer[i].x + 'px';
				_me._layer[i].reverse = false;
			}
		}
		for(var i = 0; i < _me._layer.length; i++)
		{
			var current_layer = _me._layer[i];
			if(current_layer.level > 1)
			{
				var layer_width  = current_layer.outerwidth;
				var border_width = current_layer.border;
				var layer_absx   = findPos(current_layer.handler)[0];
				if((layer_absx + layer_width + border_width*current_layer.level - border_width) > max_right && _direction)
				{
					current_layer.handler.style.left = -layer_width - _shiftx + 'px';
					current_layer.reverse = true;
				}
			}
		}
	}
	this.pcloseall = function() { closeall(); };
	this.reset = function() { setpositions(getClientWidth(), getScrollLeft()); };
	this.eventresize = function() { setpositions(getClientWidth(), getScrollLeft()); };
	this.eventscroll = function() { setpositions(getClientWidth(), getScrollLeft()); };
	this.eventover = function()
	{
		if(_mouseout)
		{
			_mouseout = false;
			mcancelclosetime();
			var currentli = this;
			var layer = currentli.getElementsByTagName("ul")[0];
			var ind = getlayerindex(layer);
			if(ind >= 0) mopen(ind);
			var open_layers = new Array();
				open_layers[0] = currentli.getElementsByTagName("ul")[0];
				if(!open_layers[0]) open_layers[0] = 0;
				var currobj = currentli.parentNode;
				var num = 0;
				while(currobj.id != PBDDM_ID)
				{
					if(currobj.nodeName == 'UL')
					{
						num++;
						open_layers[num] = currobj;
					}
					currobj = currobj.parentNode;
				}
			var layers_to_hide = new Array(_me._layer.length);
			for(var i=0; i < layers_to_hide.length; i++)
				layers_to_hide[i] = false;
			for(var i=0; i < open_layers.length; i++)
				layers_to_hide[getlayerindex(open_layers[i])] = true;
			for(var i=0; i < layers_to_hide.length; i++)
				if(!layers_to_hide[i] && (_currentlayer != open_layers[0])) mclose(i);
			_currentlayer = open_layers[1];
		}
	};
	this.eventout = function() { _mouseout = true; };
	this.allout   = function() { mclosetime(); };
	this.allover  = function() { mcancelclosetime(); };
	if(document.getElementById('debug')) _debug = document.getElementById('debug');
	_debug.value = '';
	//var css = _handler.style.cssText;
	//_handler.style.cssText = 'visibility:visible;float:left;border-width:0px;margin:0;padding:0;';
	//_handler.style.cssText = ';width:'+_handler.offsetWidth+'px;'+'height:'+_handler.offsetHeight+'px;'+css;
	var all_li = _handler.getElementsByTagName("li");
	this._layer[0] = new layer(_handler);
	for(var z = 0; z < all_li.length; z++)
	{
		var layer_handler = all_li[z].getElementsByTagName("ul")[0];
		if(layer_handler) this._layer[this._layer.length] = new layer(layer_handler);
		all_li[z].onmouseover = this.eventover;
		all_li[z].onmouseout  = this.eventout;
	}
	_handler.onmouseout  = this.allout;
	_handler.onmouseover = this.allover;
	if(_direction) window.onresize = this.eventresize;
	window.onscroll = this.eventscroll;
	for(var num = 1; num < this._layer.length; num++)
	{
		var nodesww = this._layer[num].handler.childNodes;
		var nodes = new Array();
		var specific_nodes = new Array();
		var maxwidth = 0;
		for(var x = 0; x < nodesww.length; x++)
		{	if(!is_ignorable(nodesww[x])) nodes[nodes.length] = nodesww[x]; }
		for(var y = 0; y < nodes.length; y++)
		{
		    var dnodes = nodes[y].getElementsByTagName("*");
			if(dnodes.length && !is_ignorable(dnodes[0]) && dnodes[0].nodeName != 'A')
			{
				dnodes[0].style.display = 'none';
				specific_nodes[specific_nodes.length] = dnodes[0];
			}
		}
		for(var z = 0; z < nodes.length; z++)
		{
			var anodes = nodes[z].getElementsByTagName("a");
			if(anodes[0])
			{
				var width = anodes[0].offsetWidth;
				if(width > maxwidth) maxwidth = width;
			}
		}
		for(var s = 0; s < specific_nodes.length; s++) specific_nodes[s].style.display = 'block';
		this._layer[num].handler.style.width = maxwidth + 'px';
	}
	for(var num = 0; num < this._layer.length; num++)
	{
		var current_layer = this._layer[num];
		current_layer.level       = getlevel(current_layer.handler);
		current_layer.parentindex = getparentindex(current_layer.handler);
		current_layer.outerwidth  = current_layer.handler.offsetWidth;
		current_layer.outerheight = current_layer.handler.offsetHeight;
		current_layer.innerwidth  = getchildnode(current_layer.handler.getElementsByTagName("li")[0]).offsetWidth;
		current_layer.innerheight = 0;
		current_layer.border      = (current_layer.outerwidth - current_layer.innerwidth)/2;
		current_layer.topmargin   = gettopmargin(current_layer.handler);
		current_layer.shifter     = getparentheight(current_layer.handler);
	}
	for(var num = 0; num < this._layer.length; num++)
	{
		var level = this._layer[num].level;
		var current_layer = this._layer[num];
		if((_orientation == 'h' && level > 1) || (_orientation == 'v' && level > 0))
		{
			current_layer.x = this._layer[current_layer.parentindex].innerwidth + _shiftx;
			current_layer.y = current_layer.handler.offsetTop - current_layer.topmargin - current_layer.shifter  + _shifty;
			current_layer.handler.style.left = current_layer.x + 'px';
			current_layer.handler.style.top  = current_layer.y + 'px';
		}
	}
	setpositions(getClientWidth(), getScrollLeft());
}
function changeOpac(obj_num, layer_num, opacity)
{
	var object = obj_menu[obj_num];
	var layer  = object._layer[layer_num];
	layer.degree = opacity;
	layer.handler.style.opacity			= (opacity / 100);
	layer.handler.style.MozOpacity		= (opacity / 100);
	layer.handler.style.KhtmlOpacity	= (opacity / 100);
	layer.handler.style.filter			= "alpha(opacity="+opacity+")";
	if(opacity > 98) layer.handler.style.filter = 'none';
	if(opacity  > 0) layer.handler.style.visibility='visible';
	if(opacity <= 0) layer.handler.style.visibility='hidden';
}
function changePOS(obj_num, layer_num, pos, ori)
{
	var object		= obj_menu[obj_num];
	var layer		= object._layer[layer_num];
	var level		= layer.level;
	var width		= layer.outerwidth;
	var height		= layer.outerheight;
	var margintop	= layer.topmargin;
	var reverse		= layer.reverse;
	layer.degree = pos;
	if(!reverse)
	{
		if(level == 1 && ori == 0)
		{
			var h = height - pos*height/100;
			uniclip(layer.handler, h, 2000, 2000, 0);
			layer.handler.style.marginTop = -h+margintop+'px';
		}
		else
		{
			var w = width - pos*width/100;
			uniclip(layer.handler, 0, 2000, 2000, w);
			layer.handler.style.marginLeft = -w+'px';
		}
	}
	else
	{
		var w = width - pos*width/100;
		var mw = width - w;
		uniclip(layer.handler, 0, mw, 2000, 0);
		layer.handler.style.marginLeft = w+'px';
	}
	if(pos <= 0)
	{
		layer.handler.style.visibility = 'hidden';
		uniclip(layer.handler, 0, 0, 0, 0);
		layer.handler.style.marginLeft = 'auto';
	}
	if(pos  > 0)
	{
		layer.handler.style.visibility = 'visible';
	}
	if(pos  > 98)
	{
		uniclip(layer.handler, 0, 0, 0, 0);
		layer.handler.style.marginLeft = 'auto';
	}
}
function mlddmreset() { for(var i=0; i < obj_menu.length; i++) { obj_menu[i].reset(); } }
function mlddmclose() { for(var i=0; i < obj_menu.length; i++) { obj_menu[i].pcloseall(); } }
document.onclick = mlddmclose;
function is_all_ws(nod)
{
	return !(/[^\t\n\r ]/.test(nod.data));
}
function is_ignorable(nod)
{
	return ( nod.nodeType == 8) ||
           ((nod.nodeType == 3) && is_all_ws(nod) );
}
function node_after(sib)
{
	while((sib = sib.nextSibling)) { if(!is_ignorable(sib)) return sib; }
	return null;
}
function getchildnode(handler)
{
	var node = handler.childNodes[0];
	if(is_ignorable(node)) node = node_after(node);
	return node;
}
function uniclip(handler, x1, y1, x2, y2)
{
	if((x1 == 0) && (y1 == 0) && (x2 == 0) && (y2 == 0))
	{
		var csstext = handler.style.cssText;
		handler.style.cssText = csstext.replace(/clip: {0,2}.*\);{0,1}/i, '');;
		return;
	}
	handler.style.clip = 'rect('+x1+'px, '+y1+'px, '+x2+'px, '+y2+'px)';
}
function getClientWidth()
{
	return document.documentElement.clientWidth;
}
function getClientHeight()
{
	return document.documentElement.clientHeight;
}
function getScrollLeft()
{
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.scrollLeft:document.body.scrollLeft;
}
function findPos(obj)
{
	var curleft = curtop = 0;
	if(obj.offsetParent)
	{
		do
		{
			curleft += obj.offsetLeft;
			curtop  += obj.offsetTop;
		}
		while(obj = obj.offsetParent);
	}
	return [curleft,curtop];
}