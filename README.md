HTTP to HTTPS Redirect has been configured in Cloudflare's "Page Rules".

Reason for using Cloudflare on this project:

• .htaccess file redirect causes a redirect loop that never ends (the browser is not able to display the website)
• Adding a redirect using PHP code causes a redirect loop that never ends (the browser is not able to display the website)
• The code for this website is very old and causing redirect loops when redirecting from http to https